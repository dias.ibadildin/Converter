package com.bigd.convertertry1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;



public class MainActivity extends AppCompatActivity {
    Button btn1;
    EditText number;
    EditText result;
    int number1;
    int res1;
    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        number = (EditText) findViewById(R.id.numb);
        result = (EditText) findViewById(R.id.res);
        btn1 = (Button) findViewById(R.id.button);
        btn1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                if(!number.getText().toString().matches("")){
                number1 = Integer.parseInt(number.getText().toString());
                res1 = number1*1000;
                result.setText(Integer.toString(res1));
            }}
        });

    }
}
